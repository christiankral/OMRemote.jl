using OMJulia, Base

function loadFile(omc, file; verbose = false)
    if file == ""
        # Nothing to do if nothing is provided
    else
        if isfile(file)
            if verbose
                print("    loadFile(\"" * file * "\")")
            end
            status = OMJulia.sendExpression(omc, "loadFile(\"" * file * "\")")
            if verbose
                if status == true
                    println(" successful")
                else
                    println(" failed")
                end
            end
        else
            Error("loadFile: File " * file * "not found")
        end
    end
end
